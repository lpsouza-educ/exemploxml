﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace ExemploXML
{
    class Program
    {
        static void Main(string[] args)
        {
            XDocument xml = XDocument.Load(@"exemplo.xml");
            IEnumerable<Pessoa> pessoas = xml.Root
                .Descendants("Pessoa")
                .Select(pessoa => new Pessoa
                {
                    Nome = pessoa.Element("Nome")?.Value,
                    Nascimento = new DateTime(
                        int.Parse(pessoa.Element("Nascimento")?.Value.Substring(0,4)),
                        int.Parse(pessoa.Element("Nascimento")?.Value.Substring(5,2)),
                        int.Parse(pessoa.Element("Nascimento")?.Value.Substring(8,2))
                    )
                });

            foreach (Pessoa p in pessoas)
            {
                Console.WriteLine(string.Format("{0} - {1}", p.Nome, p.Nascimento.ToShortDateString()));
            }
        }
    }
}
